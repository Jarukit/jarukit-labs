package panjarak.jarukit.lab7;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class AthleteFormV3 extends AthleteFormV2 {
	
	protected JPanel typePanel;
	protected JLabel typeLabel;
	protected JComboBox<String> typeCmboBox;
	
	protected JMenuBar menuBar;
	protected JMenu fileMenu, configMenu;
	
	protected JMenuItem newMnuItm, openMnuItm, saveMnuItm, exitMnuItm;
	protected JMenuItem colorMnuItm, sizeMnuItm;
	
	public AthleteFormV3(String title) {
		super(title);
	}

	
	protected void initComponents() {
		super.initComponents();
		
		typePanel = new JPanel(new GridLayout(1,2));
		
		typeLabel = new JLabel("Type:");
		
		typeCmboBox = new JComboBox<String>();
		typeCmboBox.addItem("Badminton player");
		typeCmboBox.addItem("Boxer");
		typeCmboBox.setSelectedItem("Boxer");
		typeCmboBox.addItem("Footballer");
		
	}
	
	protected void addComponents() {
		super.addComponents();
		
		typePanel.add(typeLabel);
		typePanel.add(typeCmboBox);
		
		overallPanel.add(typePanel, BorderLayout.CENTER);
	}
	
	protected void initMenus() {

        menuBar = new JMenuBar();
        
        fileMenu = new JMenu("File");
        configMenu = new JMenu("Config");

    	newMnuItm = new JMenuItem("New");
    	openMnuItm = new JMenuItem("Open");
    	saveMnuItm = new JMenuItem("Save");
    	exitMnuItm = new JMenuItem("Exit");
    	colorMnuItm = new JMenuItem("Color");
    	sizeMnuItm = new JMenuItem("Size");
    	
	}
	
	protected void addMenus() {
		initMenus();
		
		fileMenu.add(newMnuItm);
		fileMenu.add(openMnuItm);
		fileMenu.add(saveMnuItm);
		fileMenu.add(exitMnuItm);
		configMenu.add(colorMnuItm);
		configMenu.add(sizeMnuItm);
		
		menuBar.add(fileMenu);
		menuBar.add(configMenu);
		
		setJMenuBar(menuBar);
		
	}
	public static void createAndShowGUI(){
		AthleteFormV3 patientForm3 = new AthleteFormV3("Patient Form V3");
		patientForm3.addComponents();
		patientForm3.addMenus();
		patientForm3.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
