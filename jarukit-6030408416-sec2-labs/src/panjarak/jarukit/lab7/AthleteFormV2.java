package panjarak.jarukit.lab7;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class AthleteFormV2 extends AthleteFormV1{
	protected JPanel genderPanel, cmpPanel, contentPanel, genderChoicesPanel;
	protected JLabel genderLabel, cmpLabel;
	protected JRadioButton maleRdoBtn, fmaleRdoBtn;
	protected ButtonGroup genderBGrp;
	protected JTextArea cmpTxtArea;
	protected JScrollPane cmpScrollPane;
	public final static String FEMALE = "Female";
	public final static String MALE = "Male";
	public final static int NUM_TXTSAREA_ROWS = 2;
	public final static int NUM_TXTSAREA_COLS = 35;
	public AthleteFormV2(String title) {
		super(title);
	}
	
	protected void initComponents() {
		super.initComponents();
		
		contentPanel = new JPanel(new BorderLayout());
		genderPanel = new JPanel(new GridLayout(1,2));
		genderChoicesPanel = new JPanel();
		cmpPanel = new JPanel(new BorderLayout());
		
		genderLabel = new JLabel("Gender:");
		maleRdoBtn = new JRadioButton(MALE);
		fmaleRdoBtn = new JRadioButton(FEMALE);
		genderBGrp = new ButtonGroup();
		genderBGrp.add(maleRdoBtn);
		genderBGrp.add(fmaleRdoBtn);
		maleRdoBtn.setSelected(true);
		
		cmpLabel = new JLabel("Competition:");
		cmpTxtArea = new JTextArea(NUM_TXTSAREA_ROWS, NUM_TXTSAREA_COLS);
		cmpTxtArea.setLineWrap(true);
		cmpTxtArea.setWrapStyleWord(true);
		cmpTxtArea.setText("Competed in the 31st national championship");
		cmpScrollPane = new JScrollPane(cmpTxtArea);
	}
	
	protected void addComponents() {
		super.addComponents();
		genderChoicesPanel.add(maleRdoBtn);
		genderChoicesPanel.add(fmaleRdoBtn);
		genderPanel.add(genderLabel);
		genderPanel.add(genderChoicesPanel);
		
		cmpPanel.add(cmpLabel, BorderLayout.NORTH);
		cmpPanel.add(cmpScrollPane, BorderLayout.SOUTH);
		
		contentPanel.add(textsPanel, BorderLayout.NORTH);
		contentPanel.add(genderPanel, BorderLayout.CENTER);
		contentPanel.add(cmpPanel, BorderLayout.SOUTH);
		
		overallPanel.add(contentPanel, BorderLayout.CENTER);
	}
	
	public static void createAndShowGUI(){
		AthleteFormV2 athleteForm2 = new AthleteFormV2("Athlete Form V2");
		athleteForm2.addComponents();
		athleteForm2.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
