package panjarak.jarukit.lab7;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MySimpleWindow extends JFrame{
	protected JPanel buttonsPanel;
	protected JButton cancelButton, okButton;
	
	public MySimpleWindow(String title) {
		super(title);
	}

	
	protected void  addComponents() {
		
		buttonsPanel = new JPanel();

		cancelButton = new JButton("Cancle");
		okButton = new JButton("OK");
		
		buttonsPanel.add(cancelButton);
		buttonsPanel.add(okButton);
		
		this.setContentPane(buttonsPanel);
		
	}
	
	public void setFrameFeatures() {
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = this.getWidth();
		int h = this.getHeight();
		int x = (dim.width - w)/2;
		int y = (dim.height - h)/2;
		setLocation(x,y);
		setDefaultLookAndFeelDecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	public static void createAndShowGUI(){
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
