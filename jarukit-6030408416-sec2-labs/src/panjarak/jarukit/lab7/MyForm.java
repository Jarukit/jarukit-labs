package panjarak.jarukit.lab7;


import java.awt.EventQueue;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class MyForm extends JFrame {
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				MyForm frame = new MyForm();
				frame.setVisible(true);
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MyForm() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 362, 249);
		setTitle("ThaiCreate.Com Java GUI Tutorial");
		getContentPane().setLayout(null);
		
		// Button 1
		JButton btn1 = new JButton("Button 1");
		btn1.setBounds(129, 49, 99, 23);
		getContentPane().add(btn1);
		
		// Button 2
		JButton btn2 = new JButton();
		btn2.setText("Button 2");
		btn2.setBounds(111, 74, 128, 23);
		getContentPane().add(btn2);
		
		// Button 3 (Icon)
		JButton btn3 = new JButton(new ImageIcon(getClass().getResource("save.gif")));
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {			
				JOptionPane.showMessageDialog(null,
						"Hello");	
			}
		});
		btn3.setBounds(156, 119, 46, 39);
		getContentPane().add(btn3);

	}

}