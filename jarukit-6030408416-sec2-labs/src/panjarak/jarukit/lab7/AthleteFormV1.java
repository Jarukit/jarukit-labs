package panjarak.jarukit.lab7;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;

public class AthleteFormV1 extends MySimpleWindow {
	protected JPanel namePanel, datePanel, weightPanel, heightPanel, 
					 nationalityPanel;
	protected JLabel nameLabel, dateLabel, weightLabel, heightLabel,
					 nationalityLabel;
    protected JTextField nameTextField, dateTextField, weightTextField, 
    					 heightTextField, nationalityTextField;
    
    protected JPanel overallPanel, textsPanel;
    
	private static final int TOOLTIP_INIT_DELAY = 100;
	private static final int TOOLTIP_DISMISS_DELAY = 5000;
	
	public final static int TXTFIELD_LENGTH = 15;

	public AthleteFormV1(String title) {
		super(title);
		ToolTipManager.sharedInstance().setInitialDelay(TOOLTIP_INIT_DELAY);
	    ToolTipManager.sharedInstance().setDismissDelay(TOOLTIP_DISMISS_DELAY);
	}
	
	protected void initComponents() {
		textsPanel = new JPanel(new GridLayout(5,1));
		overallPanel = new JPanel(new BorderLayout());
		
		namePanel = new JPanel(new GridLayout(1,2));
		datePanel = new JPanel(new GridLayout(1,2));
		weightPanel = new JPanel(new GridLayout(1,2));
		heightPanel = new JPanel(new GridLayout(1,2));
		nationalityPanel = new JPanel(new GridLayout(1,2));
		
		nameLabel = new JLabel("Name:");
		nameTextField = new JTextField(TXTFIELD_LENGTH);
		dateLabel = new JLabel("Birthdate:");
		dateTextField = new JTextField(TXTFIELD_LENGTH);
		dateTextField.setToolTipText("ex. 22.02.2000");
		weightLabel = new JLabel("Weight (kg.):");
		weightTextField = new JTextField(TXTFIELD_LENGTH);
		heightLabel = new JLabel("Height (metre):");
		heightTextField = new JTextField(TXTFIELD_LENGTH);
		nationalityLabel = new JLabel("Nationality:");
		nationalityTextField = new JTextField(TXTFIELD_LENGTH);
	
	}
	
	protected void setLabelTxtField(JPanel panel,
			JLabel label, JTextField textField) {
		panel.add(label);
		panel.add(textField);
	}
	
	protected void addComponents() {
		super.addComponents();
		initComponents();
		
		setLabelTxtField(namePanel, nameLabel, nameTextField);
		textsPanel.add(namePanel);
		setLabelTxtField(datePanel, dateLabel, dateTextField);
		textsPanel.add(datePanel);
		setLabelTxtField(weightPanel, weightLabel, weightTextField);
		textsPanel.add(weightPanel);
		setLabelTxtField(heightPanel, heightLabel, heightTextField);
		textsPanel.add(heightPanel);
		setLabelTxtField(nationalityPanel, nationalityLabel, 
						 nationalityTextField);
		textsPanel.add(nationalityPanel);
		
		overallPanel.add(textsPanel, BorderLayout.NORTH);
		overallPanel.add(buttonsPanel, BorderLayout.SOUTH);
		setContentPane(overallPanel);
		
	}

	public static void createAndShowGUI(){
	AthleteFormV1 athleteForm1 = new AthleteFormV1("Athlete Form V1");
	athleteForm1.addComponents();
	athleteForm1.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
