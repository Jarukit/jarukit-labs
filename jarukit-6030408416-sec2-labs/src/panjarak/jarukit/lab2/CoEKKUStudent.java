package panjarak.jarukit.lab2;
public class CoEKKUStudent {
	public static void main(String[] args) {
		if (args.length == 5) {
			String yourName = args[0];
			long yourID = Long.parseLong(args[1]);
			Double yourGPA = Double.parseDouble(args[2]);
			String yourYear = args[3];
			Double yourIncome = Double.parseDouble(args[4]);
			long  firstCoeID = 3415717L;
			
			System.out.println(yourName + " has ID = " + yourID + " GPA = " + yourGPA + " year = " + yourYear + " parent's income = " + yourIncome);
			System.out.println("Your ID is different from the first COE student's ID by " + (yourID - firstCoeID));
		}
		else {
			System.err.println("CoEKKUStudent <name> <ID> <GPA> <academic year> <parents' income>");
		}
	}

}
