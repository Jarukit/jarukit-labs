package panjarak.jarukit.lab2;
public class Calculator {
	public static void main(String[] args) {
		if (args.length <= 2) {
			System.err.println("Calculator <operand1> <operand2> <operator>");
		}
		else {
			Double decimalNumber_1 = Double.parseDouble(args[0]);
			Double decimalNumber_2 = Double.parseDouble(args[1]);
			if (args[2].equals("*") || args[2].equals(".classpath")) {
				double answer = decimalNumber_1 * decimalNumber_2 ;
				System.out.println(decimalNumber_1 + " * " + decimalNumber_2 + " = " +  answer );
			}
			else if (args[2].equals("+")) {
				double answer = decimalNumber_1 + decimalNumber_2 ;
				System.out.println(decimalNumber_1 + " + " + decimalNumber_2 + " = " +  answer );
			}
			else if (args[2].equals("-")) {
				double answer = decimalNumber_1 - decimalNumber_2 ;
				System.out.println(decimalNumber_1 + " - " + decimalNumber_2 + " = " +  answer );
			}
			else if (args[2].equals("/")) {
				double answer = decimalNumber_1 / decimalNumber_2 ;
				if (decimalNumber_2 == 0) {
					System.out.println("The second operator cannot be zero");
				}
				else {
				System.out.println(decimalNumber_1 + " / " + decimalNumber_2 + " = " +  answer );
				}
			}
		}
				
		
	}
}

