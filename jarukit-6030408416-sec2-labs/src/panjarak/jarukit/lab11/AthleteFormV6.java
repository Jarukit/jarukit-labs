package panjarak.jarukit.lab11;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class AthleteFormV6 extends AthleteFormV5 implements ActionListener{

	private static final long serialVersionUID = 1L;
	protected  JLabel picture;
	protected JPanel panelAthV6;
	
	public AthleteFormV6(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
	//add image icon to menuitem 
	private void addImgIcon() {
		ImageIcon iconOpen = new ImageIcon(getURL("openIcon"));
		ImageIcon iconSave = new ImageIcon(getURL("quitIcon"));
		ImageIcon iconExit = new ImageIcon(getURL("saveIcon"));
		menuItemOpen.setIcon(iconOpen);
		menuItemSave.setIcon(iconSave);
		menuItemExit.setIcon(iconExit);
		
	}

	//get path of image for menu icon
	protected URL getURL(String filename) {
		String imgFile = "photo/" + filename + ".png";
		URL url = this.getClass().getResource(imgFile);
		return url;
	}

	protected void showImg() {
		//Set up the picture.
		picture = new JLabel();
	    picture.setHorizontalAlignment(JLabel.CENTER);
	    // top = 20, left = 0, bottom = 0, right = 0
	    picture.setBorder(BorderFactory.createEmptyBorder(10,0,10,0));
		panelAthV6 = new JPanel(new BorderLayout());
		//set position 
		updatePicture("Badminton player");//set default image
		panelAthV6.add(picture, BorderLayout.PAGE_START);//North
		panelAthV6.add(mainPanel4, BorderLayout.PAGE_END);//South
		setContentPane(panelAthV6);
		
	}
	
	//
	protected void updatePicture(String sportType) {
		//get path for image of athlete
    	String imgFile = "photo/" + sportType + ".jpg";
		try {
			URL file = this.getClass().getResource(imgFile);
			ImageIcon icon = new ImageIcon(file);
			//update image 
			picture.setIcon(icon);
		} catch (Exception e) {
			
		}	
	}
	
	protected void addListeners() {
		super.addListeners();
		athleteType.addActionListener(this);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		// when the user chooses athlete type 
		if (event.getSource() == athleteType) {
			 @SuppressWarnings("unchecked")
			JComboBox<String> cb = (JComboBox<String>)event.getSource();
		    String atTypecom = (String)cb.getSelectedItem();
		    updatePicture(atTypecom);
		 }
	 
	}

	@Override
	public void addComponents() {
		super.addComponents();
		showImg();
		addImgIcon();
		
	}
	
	
	
	public static void createAndShowGUI() {
		AthleteFormV6 AthleteForm6 = new AthleteFormV6("Athlete Form V6");
		AthleteForm6.addComponents();
		AthleteForm6.addListeners();
		AthleteForm6.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
