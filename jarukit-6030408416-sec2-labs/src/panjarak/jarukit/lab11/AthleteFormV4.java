package panjarak.jarukit.lab11;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;

public class AthleteFormV4 extends AthleteFormV3 implements ActionListener, ItemListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 501930691139901380L;
	protected String  gender, type;
	protected JButton okbuttonJdialog;
	protected JPanel paneDialog;
	protected JOptionPane op;
	protected String msg = "";
	public AthleteFormV4(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	//handle radiobutton
	protected void handleGender(ActionEvent event) {
    	// type casting from Object to JRadioButton
		JRadioButton gender = (JRadioButton) event.getSource();
		if (gender.isSelected()) {
			showMsg("You gender type is now changed to " + gender.getText());
		}
	}
	
	//(Toolkit.getDefaultToolkit().getScreenSize().height + getSize().height) / 2 + 20 )
	protected void showMsg(String text) {
		//create dialog and set up position 
		op = new JOptionPane();
		op.setMessage(text); //set text
		JDialog d = op.createDialog(this, "Gender info"); //set frame and title
		d.setLocation((Toolkit.getDefaultToolkit().getScreenSize().width - d.getWidth())/2, this.getHeight() + this.getLocation().y + 20); //set position
        d.setVisible(true);
	}
	
	protected void handleathleteType(ItemEvent event) {
		// type casting from Object to JComboBox
		@SuppressWarnings("unchecked")
		JComboBox<String> cb = (JComboBox<String>) event.getItemSelectable();
		if (event.getStateChange() == ItemEvent.SELECTED) { // Get the selected item
			JOptionPane.showMessageDialog(this, "You athilte is change to " + cb.getSelectedItem().toString());
		}
	}

	protected void handleCancelButton() {
		//set resets all text fields and a text area to empty strings
		nameTf.setText("");
		birthdateTf.setText("");
		weigthTf.setText("");
		heightTf.setText("");
		natTf.setText("");
		textarea.setText("");
		radioButMale.setSelected(true);
		athleteType.setSelectedIndex(0);
	}

	protected void handleOKButton() {
		//get Text form combobox and radiobutton
		if (nameTf.getText().length() == 0 || weigthTf.getText().length() == 0 || heightTf.getText().length() == 0 ||
			natTf.getText().length() == 0 ) {
			JOptionPane.showMessageDialog(this, "pls enter your infomation ", null, JOptionPane.WARNING_MESSAGE);
		}else {
			type = athleteType.getSelectedItem().toString();
			if (radioButMale.isSelected()) {
				gender = radioButMale.getText();

			} else if (radioButFemale.isSelected()) {
				gender = radioButFemale.getText();
			}

			msg += "name = " + nameTf.getText() + ",Birthdate = " + birthdateTf.getText() + ",Weight = "
					+ weigthTf.getText() + ",Height = " + heightTf.getText() + ",Nationality = " + natTf.getText()
					+ "\n" + "Gender = " + gender + "\n" + "Competition = " + textarea.getText() + "\n" + "Type = "
					+ type;

			// show message dialog
			JOptionPane.showMessageDialog(this, msg);
		}
		

	}
	
	//ActionListener control OK Button, Cancel Button and radiobutton
		@Override
		public void actionPerformed(ActionEvent event) {
			// get source that activates the event
			Object src = event.getSource();
			if (src == okB) {
				handleOKButton();
			} else if (src == cancelB) {
				handleCancelButton();
			} else if (src == radioButMale || src == radioButFemale ) {
				handleGender(event);
			} 

		}

		//ItemListener control athlete type  (Combobox)
		@Override
		public void itemStateChanged(ItemEvent event) {
			// get source that activates the event
			Object source = event.getSource();
			if (source == athleteType) {
				handleathleteType(event);
			}
		}

	//add Listener with component
	protected void addListeners() {
		okB.addActionListener(this);
		cancelB.addActionListener(this);
		radioButMale.addActionListener(this);
		radioButFemale.addActionListener(this);
		athleteType.addItemListener(this);
	}

	public static void createAndShowGUI() {
		AthleteFormV4 AthleteForm4 = new AthleteFormV4("Athlete Form V4");
		AthleteForm4.addComponents();
		AthleteForm4.addListeners();
		AthleteForm4.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
