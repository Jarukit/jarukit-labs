package panjarak.jarukit.lab11;

import java.awt.*;
import javax.swing.*;

class AthleteFormV1 extends MySimpleWindow {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JPanel textFieldPanel1, textFieldPanel2, textFieldPanel3, textFieldPanel4, textFieldPanel5, labeTfPanel,
			mainPanel1;
	protected JTextField nameTf, birthdateTf, weigthTf, heightTf, natTf;
	final int textWidth = 15;

	public AthleteFormV1(String string) {
		// call the constructor of superclass
		// that accepts string
		super(string);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void addComponents() {
		super.addComponents();
		GridLayout layout = new GridLayout(1, 2);
		// panel TextField and Label
		textFieldPanel1 = new JPanel(layout);
		textFieldPanel2 = new JPanel(layout);
		textFieldPanel3 = new JPanel(layout);
		textFieldPanel4 = new JPanel(layout);
		textFieldPanel5 = new JPanel(layout);

		// add component Label and TeaxtField
		textFieldPanel1.add(new JLabel("Name:"));
		nameTf = new JTextField(textWidth);
		textFieldPanel1.add(nameTf);

		textFieldPanel2.add(new JLabel("Birthdate:"));
		birthdateTf = new JTextField(textWidth);
		// set tootip text
		birthdateTf.setToolTipText("ex. 22.02.2000");
		textFieldPanel2.add(birthdateTf);

		textFieldPanel3.add(new JLabel("Weight (kg):"));
		weigthTf = new JTextField(textWidth);
		textFieldPanel3.add(weigthTf);

		textFieldPanel4.add(new JLabel("Height (metre):"));
		heightTf = new JTextField(textWidth);
		textFieldPanel4.add(heightTf);

		textFieldPanel5.add(new JLabel("Nationality:"));
		natTf = new JTextField(textWidth);
		textFieldPanel5.add(natTf);

		// add all textfieldpanel
		labeTfPanel = new JPanel(new GridLayout(5, 2));
		labeTfPanel.add(textFieldPanel1);
		labeTfPanel.add(textFieldPanel2);
		labeTfPanel.add(textFieldPanel3);
		labeTfPanel.add(textFieldPanel4);
		labeTfPanel.add(textFieldPanel5);

		// add all component in mainPanel1
		mainPanel1 = new JPanel(new BorderLayout());
		mainPanel1.add(labeTfPanel, BorderLayout.NORTH);
		mainPanel1.add(panelButOkCalcel, BorderLayout.SOUTH);
		setContentPane(mainPanel1);

	}

	public static void createAndShowGUI() {
		AthleteFormV1 athleteForm1 = new AthleteFormV1("Athlete Form V1");
		athleteForm1.addComponents();
		athleteForm1.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}