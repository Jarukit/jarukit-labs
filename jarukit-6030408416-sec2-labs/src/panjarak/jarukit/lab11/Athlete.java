
package panjarak.jarukit.lab11;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

// super class or parent class
public class Athlete {

	private String name;
	private double weight;
	private double height;
	private Gender gender;
	private String nationality;
	private LocalDate birthdate;

	/**
	 * This constructor is used to initialize all private variables of class Athlete
	 * 
	 * @param name
	 *            Firstname and lastname
	 * @param weight
	 *            Weight in kilograms
	 * @param height
	 *            Height in meters
	 * @param gender
	 *            Using enum with values as MALE or FEMALE
	 * @param nationality
	 *            Full nationality name, such as Thai
	 * @param birthdate
	 *            Date of birth in format 31/12/2017
	 */

	// Constructor
	public Athlete(String name, double weight, double height, Gender gender, String nationality, String birthdate) {
		setName(name);
		setWeight(weight);
		setHeight(height);
		setGender(gender);
		setNationality(nationality);
		setBirthdate(birthdate);

	}
	//constructor for class ThailandGoTournament
	public Athlete(String name, double weight, double height, Gender gender, String birthdate) {
		setName(name);
		setWeight(weight);
		setHeight(height);
		setGender(gender);
		setBirthdate(birthdate);

	}
	
	// getter setter
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		this.birthdate = LocalDate.parse(birthdate, formatter);

	}

	public void playSport() {
		String name = getName();
		System.out.println(name + " is good at sport.");
	}

	// returns a string representation of the object.
	@Override
	public String toString() {
		return "Athlete [" + name + ", " + weight + "kg, " + height + "m, " + gender + ", " + nationality + ", "
				+ birthdate + "]";
	}

}// end of class
