package panjarak.jarukit.lab11;

import java.awt.*;
import javax.swing.*;


public class AthleteFormV2 extends AthleteFormV1 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JPanel radioButPanel, radioButMainPanel, textAreaPanel, 
					radTextAreaPanel, labeTaPanel, mainPanel2;
	protected JRadioButton radioButMale, radioButFemale;
	protected JTextArea textarea;

	public AthleteFormV2(String string) {
		// call the constructor of superclass
		// that accepts string
		super(string);
	}

	@Override
	public void addComponents() {
		super.addComponents();
		// JRadioButom
		radioButMale = new JRadioButton("Male", true);
		radioButFemale = new JRadioButton("Female");
		// Panel radiobuttom male and female
		radioButPanel = new JPanel();
		ButtonGroup choices = new ButtonGroup();
		choices.add(radioButMale);
		choices.add(radioButFemale);
		radioButPanel.add(radioButMale);
		radioButPanel.add(radioButFemale);

		// add panel Radiobuttom and Label
		radioButMainPanel = new JPanel(new GridLayout(1, 2));
		radioButMainPanel.add(new JLabel("Gender:"));
		radioButMainPanel.add(radioButPanel);

		// JtextArea
		textarea = new JTextArea(2, 35);
		textarea.setText("Competed in the 31st national championship");
		textarea.setLineWrap(true); // setLineWrap
		textarea.setWrapStyleWord(true);// cut word
		JScrollPane scroller = new JScrollPane(textarea);
		scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		// textArea panel add scroller and label
		textAreaPanel = new JPanel(new BorderLayout());
		textAreaPanel.add(new JLabel("Competition"), BorderLayout.NORTH);
		textAreaPanel.add(scroller);

		// add radioButMainPanel and textAreaPanel in radTextAreaPanel
		radTextAreaPanel = new JPanel(new BorderLayout());
		radTextAreaPanel.add(radioButMainPanel, BorderLayout.NORTH);
		radTextAreaPanel.add(textAreaPanel, BorderLayout.CENTER);

		// add all component in main panel
		mainPanel2 = new JPanel(new BorderLayout());
		mainPanel2.add(labeTfPanel, BorderLayout.NORTH);
		mainPanel2.add(radTextAreaPanel, BorderLayout.CENTER);
		mainPanel2.add(panelButOkCalcel, BorderLayout.SOUTH);
		setContentPane(mainPanel2);

	}

	public static void createAndShowGUI() {
		AthleteFormV2 AthleteForm2 = new AthleteFormV2("Athlete Form V2");
		AthleteForm2.addComponents();
		AthleteForm2.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}