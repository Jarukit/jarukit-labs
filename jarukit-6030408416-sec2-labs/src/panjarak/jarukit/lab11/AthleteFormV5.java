package panjarak.jarukit.lab11;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;


public class AthleteFormV5 extends AthleteFormV4 implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JMenuItem blue, green, red, customCol, customSize;
	JMenuItem size16, size20, size24;
	JMenu menuColor, menuSize;

	public AthleteFormV5(String title) {
		super(title);
	}

	//create menu color and menu size 
	protected void addSubMenu() {
		
		menuConfig.removeAll(); //remove menuitem in lab7 because cannot add menuitem in menuitem
		//create menu color and menu size 
		menuColor = new JMenu("Color");
		menuSize = new JMenu("Size");
		//create menu item 
		blue = new JMenuItem("Blue");
		green = new JMenuItem("Green");
		red = new JMenuItem("Red");
		customCol = new JMenuItem("Custom...");
		//add menu item to menu color
		menuColor.add(blue);
		menuColor.add(green);
		menuColor.add(red);
		menuColor.add(customCol);

		//create menu size 
		size16 = new JMenuItem("16");
		size20 = new JMenuItem("20");
		size24 = new JMenuItem("24");
		customSize = new JMenuItem("Custom...");
		//add menu item to menu  size 
		menuSize.add(size16);
		menuSize.add(size20);
		menuSize.add(size24);
		menuSize.add(customSize);
		//add menu color and menu size to menu config
		menuConfig.add(menuColor);
		menuConfig.add(menuSize);
	}

	
	protected void addListeners() {
		super.addListeners();
		blue.addActionListener(this);
		green.addActionListener(this);
		red.addActionListener(this);
		size16.addActionListener(this);
		size20.addActionListener(this);
		size24.addActionListener(this);

	}

	//set font size 
	protected void setFontSize(int size) {
		nameTf.setFont(new Font(Font.SANS_SERIF, Font.BOLD, size));
		birthdateTf.setFont(new Font(Font.SANS_SERIF, Font.BOLD, size));
		weigthTf.setFont(new Font(Font.SANS_SERIF, Font.BOLD, size));
		heightTf.setFont(new Font(Font.SANS_SERIF, Font.BOLD, size));
		natTf.setFont(new Font(Font.SANS_SERIF, Font.BOLD, size));
		textarea.setFont(new Font(Font.SANS_SERIF, Font.BOLD, size));
	}

	//set font color 
	protected void setFontColor(Color fontColor) {
		nameTf.setForeground(fontColor);
		birthdateTf.setForeground(fontColor);
		weigthTf.setForeground(fontColor);
		heightTf.setForeground(fontColor);
		natTf.setForeground(fontColor);
		textarea.setForeground(fontColor);

	}

	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		// get source that activates the event
		Object source = event.getSource();
		// when the user chooses menu item blue 
		if (source == blue) {
			setFontColor(Color.blue);
		// when the user chooses menu item green	
		} else if (source == green) {
			setFontColor(Color.green);
		// when the user chooses menu item red
		} else if (source == red) {
			setFontColor(Color.red);
		// when the user chooses menu item size16
		} else if (source == size16) {
			setFontSize(16);
		// when the user chooses menu item size20
		} else if (source == size20) {
			setFontSize(20);
		// when the user chooses menu item size24
		} else if (source == size24) {
			setFontSize(24);
		}

	}

	@Override
	public void addComponents() {
		super.addComponents();
		addSubMenu();

	}

	public static void createAndShowGUI() {
		AthleteFormV5 AthleteForm5 = new AthleteFormV5("Athlete Form V5");
		AthleteForm5.addComponents();
		AthleteForm5.addListeners();
		AthleteForm5.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
