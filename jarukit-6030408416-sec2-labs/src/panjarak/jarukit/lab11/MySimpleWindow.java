package panjarak.jarukit.lab11;

import java.awt.*;
import javax.swing.*;



class MySimpleWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JPanel panelButOkCalcel;
	protected JButton cancelB, okB;

	public MySimpleWindow(String string) {
		super(string);
	}

	protected void addComponents() {
		panelButOkCalcel = new JPanel(); //panel for Cancel and Ok buttons
		cancelB = new JButton("Cancel");
		okB = new JButton("OK");
		panelButOkCalcel.add(cancelB);
		panelButOkCalcel.add(okB);
		setContentPane(panelButOkCalcel);
	}

	protected void setFrameFeatures() {
		pack();
		//Set the window to be located at the middle of the screen
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);

		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}