package panjarak.jarukit.lab11;

import java.awt.*;
import javax.swing.*;


public class AthleteFormV3 extends AthleteFormV2 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JPanel comboBoxPanel, mergePanel, mainPanel4;
	protected JMenuBar menuBar;
	protected JMenu menuFile, menuConfig;
	protected JMenuItem menuItemNew, menuItemOpen, menuItemSave, menuItemExit, menuItemColor, menuItemSize;
	protected JComboBox<String> athleteType;

	public AthleteFormV3(String string) {
		// call the constructor of superclass
		// that accepts string
		super(string);

	}

	@Override
	public void addComponents() {
		super.addComponents();
		// menu bar has file and config
		menuBar = new JMenuBar();
		menuFile = new JMenu("File");
		menuConfig = new JMenu("Config");

		// add file and config menu
		menuBar.add(menuFile);
		menuBar.add(menuConfig);

		// Build menuItem for menuFile has new, open, save, exit
		menuItemNew = new JMenuItem("New");
		menuItemOpen = new JMenuItem("Open");
		menuItemSave = new JMenuItem("Save");
		menuItemExit = new JMenuItem("Exit");
		// Build menuItem for menuConfig has color, size
		menuItemColor = new JMenuItem("Color");
		menuItemSize = new JMenuItem("Size");

		// add menu item in menu file
		menuFile.add(menuItemNew);
		menuFile.add(menuItemOpen);
		menuFile.add(menuItemSave);
		menuFile.add(menuItemExit);
		// add menu item in menu config
		menuConfig.add(menuItemColor);
		menuConfig.add(menuItemSize);

		// Build JComboBox has Badminton player, Boxer, Footballer
		athleteType = new JComboBox<String>();
		athleteType.addItem("Badminton player");
		athleteType.addItem("Boxer");
		athleteType.addItem("Footballer");
		athleteType.setEnabled(true);
		// add label and athlete type in comboBoxPanel
		comboBoxPanel = new JPanel(new BorderLayout());
		comboBoxPanel.add(new JLabel("Type:"), BorderLayout.WEST);
		comboBoxPanel.add(athleteType, BorderLayout.EAST);

		// a group of gendcompTextPanel button and comboBoxPanel
		mergePanel = new JPanel(new BorderLayout());
		mergePanel.add(radTextAreaPanel);
		mergePanel.add(comboBoxPanel, BorderLayout.SOUTH);

		// add all component in mainPanel4
		mainPanel4 = new JPanel(new BorderLayout());
		mainPanel4.add(labeTfPanel, BorderLayout.NORTH);
		mainPanel4.add(mergePanel, BorderLayout.CENTER);
		mainPanel4.add(panelButOkCalcel, BorderLayout.SOUTH);

		setJMenuBar(menuBar);
		setContentPane(mainPanel4);

	}
	
	

	public static void createAndShowGUI() {
		AthleteFormV3 AthleteForm3 = new AthleteFormV3("Athlete Form V3");
		AthleteForm3.addComponents();
		AthleteForm3.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}