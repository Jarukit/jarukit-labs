package panjarak.jarukit.lab11;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;


public class AthleteFormV8 extends AthleteFormV7 {
	// instance of class
	protected JMenu dataM;
	protected JMenuItem displayMI, sortMI, searchMI, removeMI;
	protected ArrayList<Athlete> athletes = new ArrayList<Athlete>(); // list of Athletes
	protected double weight, height;
	protected Gender gender;
	protected Athlete ath;
	protected Object source;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AthleteFormV8(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addComponents() {
		super.addComponents();
		addDataMenu();

	}

	private void addDataMenu() {
		// initialize menu and menu item
		dataM = new JMenu("Data");

		displayMI = new JMenuItem("Display");
		sortMI = new JMenuItem("Sort");
		searchMI = new JMenuItem("Search");
		removeMI = new JMenuItem("Remove");

		dataM.add(displayMI);
		dataM.add(sortMI);
		dataM.add(searchMI);
		dataM.add(removeMI);

		menuBar.add(dataM);

	}

	public void addListeners() {
		super.addListeners();
		displayMI.addActionListener(this);
		sortMI.addActionListener(this);

	}

	@Override
	public void actionPerformed(java.awt.event.ActionEvent event) {
		super.actionPerformed(event);
		Object source = event.getSource();
		if (source == okB) {
			addAthlete();
		} else if (source == displayMI) {
			displayAthletes();
		} else if (source == sortMI) {
			Collections.sort(athletes, new HeightComparator());
			displayAthletes();
		}

	}

	public void displayAthletes() {
		String lstMsgAth = ""; // this value is message for display
		if (athletes.size() == 0) { // check when don't have athletes
			JOptionPane.showMessageDialog(this, "No Athletes", null, JOptionPane.WARNING_MESSAGE);
		} else {
			for (int i = 0; i < athletes.size(); i++) { // get value in athletes list for display
				lstMsgAth += i + 1 + ":" + athletes.get(i).toString() + "\n";
			}
			JOptionPane.showMessageDialog(this, lstMsgAth);
		}

	}

	public void addAthlete() {
		if (radioButMale.isSelected()) {
			gender = Gender.MALE;

		} else if (radioButFemale.isSelected()) {
			gender = Gender.FEMALE;
		}
		try {
			weight = Double.parseDouble(weigthTf.getText());
			height = Double.parseDouble(heightTf.getText());
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			ath = new Athlete(nameTf.getText(), weight, height, gender, natTf.getText(), birthdateTf.getText());
			athletes.add(ath); // add object athletes to athletes list
			System.out.println(athletes);

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public static void createAndShowGUI() {
		AthleteFormV8 AthleteForm8 = new AthleteFormV8("Athlete Form V8");
		AthleteForm8.addComponents();
		AthleteForm8.addListeners();
		AthleteForm8.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	// class HeightComparator is inner class that has to compare height of athletes
	class HeightComparator implements Comparator<Athlete> {
		@Override
		public int compare(Athlete a, Athlete b) {
			return a.getHeight() < b.getHeight() ? -1 : a.getHeight() == b.getHeight() ? 0 : 1;
		}
	}

}
