package panjarak.jarukit.lab11;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.io.File;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileSystemView;


public class AthleteFormV7 extends AthleteFormV6 {
	protected Color colorChooser;

	private static final long serialVersionUID = 1L;

	public AthleteFormV7(String title) {
		super(title);
	}

	public void addListeners() {
		super.addListeners();
		menuFile.addActionListener(this);
		menuItemNew.addActionListener(this);
		menuItemOpen.addActionListener(this);
		menuItemSave.addActionListener(this);
		menuItemExit.addActionListener(this);
		customCol.addActionListener(this);
		customSize.addActionListener(this);
	}

	@Override
	public void addComponents() {
		super.addComponents();
		setMnemonicAndAccelerator();

	}

	//set Mnemonic key and  Accelerator key 
	public void setMnemonicAndAccelerator() {
		menuFile.setMnemonic(KeyEvent.VK_F);

		menuItemNew.setMnemonic(KeyEvent.VK_N);
		menuItemNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, java.awt.event.ActionEvent.CTRL_MASK));

		menuItemOpen.setMnemonic(KeyEvent.VK_O);
		menuItemOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, java.awt.event.ActionEvent.CTRL_MASK));

		menuItemSave.setMnemonic(KeyEvent.VK_S);
		menuItemSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, java.awt.event.ActionEvent.CTRL_MASK));

		menuItemExit.setMnemonic(KeyEvent.VK_X);
		menuItemExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, java.awt.event.ActionEvent.CTRL_MASK));

	}

	@Override
	public void actionPerformed(java.awt.event.ActionEvent event) {
		super.actionPerformed(event);
		Object source = event.getSource();
		if (source == menuItemOpen) {
			openFileDialog();
		} else if (source == menuItemSave) {
			saveFileDialog();
		} else if (source == menuItemExit) {
			System.exit(0);
		} else if (source == customCol) {
			showColorChooser();
		}

	}

	// display Color chooser
	public void showColorChooser() {
		colorChooser = JColorChooser.showDialog(this, "Choose Color", null);
		textarea.setBackground(colorChooser);

	}

	// display save file dialog
	public void saveFileDialog() {
		JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		int returnValue = jfc.showSaveDialog(this);
		if (returnValue == JFileChooser.APPROVE_OPTION) { // Action when user choose file
			File selectedFile = jfc.getSelectedFile();
			JOptionPane.showMessageDialog(this, "Saving: " + selectedFile.getAbsoluteFile().getName());
		} else if (returnValue == JFileChooser.CANCEL_OPTION) {
			JOptionPane.showMessageDialog(this, "Saving command cancelled by user.");
		}

	}

	// display Open file dialog
	public void openFileDialog() {
		JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		int returnValue = jfc.showOpenDialog(this);

		if (returnValue == JFileChooser.APPROVE_OPTION) { // Action when user choose file
			File selectedFile = jfc.getSelectedFile();

			JOptionPane.showMessageDialog(this, "Opening: " + selectedFile.getAbsoluteFile().getName());
		} else if (returnValue == JFileChooser.CANCEL_OPTION) {
			JOptionPane.showMessageDialog(this, "Open command cancelled by user.");
		}

	}

	public static void createAndShowGUI() {
		AthleteFormV7 AthleteForm7 = new AthleteFormV7("Athlete Form V7");
		AthleteForm7.addComponents();
		AthleteForm7.addListeners();
		AthleteForm7.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
