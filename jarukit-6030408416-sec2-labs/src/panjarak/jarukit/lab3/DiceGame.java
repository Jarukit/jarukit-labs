package panjarak.jarukit.lab3;
import java.util.Scanner;
import java.util.Random;

public class DiceGame {
	public static void main(String[] args) {
		// getting input to usher.
		System.out.print("Enter your guest (1-6): ");
		Scanner scanner = new Scanner(System.in);
		int youGuest = scanner.nextInt();
		Random rand = new Random();
		int  numberRandom = rand.nextInt(6) + 1;
		// Create conditions of a game.
		if (youGuest > 0 && youGuest < 7) {
			System.out.println("You have guested number :  " + youGuest);
			System.out.println("Computer has rolled number : " + numberRandom);
			int difference = Math.abs(youGuest - numberRandom);
			if (difference == 1 || difference == 5 || difference == 0) {
				System.out.println("You win.");
				}
			else {
				System.out.println("Computer wins.");
			}
			}
		// this is conditions, that will work when input incorrect.
		else {
			System.err.println("Incorrect number. Only 1-6 can be entered."); 

		}
		}
		
	}
