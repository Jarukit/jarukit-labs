package panjarak.jarukit.lab3;
import java.util.Scanner;

public class Tamagotchi {

	public static void main(String args[]) {
		System.out.print("Enter your Tamagotchi info :");
		// Getting a text's input and Split it.
		Scanner txt = new Scanner(System.in).useDelimiter("\\s");
		// Getting three integer.
		int timeLife = txt.nextInt();
		int timeFed = txt.nextInt();
		int timeWater = txt.nextInt();
		txt.close();
		int timeCoincide = 0;
		// this is for loop.. for getting time coincide of feed and water.
		for (int i = 1; i <= timeLife ; i++) {
			int multipliedFed = timeFed * i;
			for (int j = 1; j <= timeLife; j++) {
				int multipliedWater = timeWater * j;
				if ((multipliedFed == multipliedWater) && (multipliedFed <= timeLife)) {
					timeCoincide++;
				}
			}
		}
		//  Create new variable and clear it.
		int totalAmountTimeFed = timeLife / timeFed;
		int totalAmountTimeWater = timeLife / timeWater;
		int AmountTimeFed = Math.abs(timeCoincide - totalAmountTimeFed);
		int AmountTimeWater = Math.abs(timeCoincide - totalAmountTimeWater);
		
		// print out variable.
		System.out.println("Your Tomagotchi will life for " + timeLife + " hours");
		System.out.println("It need to be fed every " + timeFed + " hours");
		System.out.println("It need to be watered every " + timeWater + " hours");
		System.out.println();
		System.out.println("You need to water-feed: " + timeCoincide + " times");
		System.out.println("You need to water: " + AmountTimeWater + " times");
		System.out.println("You need to feed: " + AmountTimeFed + " times");
		
		   }
	}