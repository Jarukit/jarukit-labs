package panjarak.jarukit.lab3;

import java.util.Random;
import java.util.Scanner;

public class DiceMethodGame {
	// this is variables.
	static int humanGuest;
	static int computerScore;
	
	public static void main(String[] args) {
		humanGuest = acceptInput();
		computerScore = genDiceRoll();
		displayWinner(humanGuest, computerScore);
		
	}
	
	// this is input usher method.
	public static int acceptInput(){
		System.out.print("Enter your guest (1-6): ");
		Scanner scanner = new Scanner(System.in);
		int youGuest = scanner.nextInt();
		return youGuest;
	}
	// this is random number method.
	public static int genDiceRoll(){
		Random rand = new Random();
		int  numberRandom = rand.nextInt(6) + 1;
		return numberRandom;
	}
	
	// this is method for display result.
	public static void displayWinner(int guest1, int guest2) {
		if (guest1 > 0 && guest1 < 7) {
			System.out.println("You have guested number :  " + guest1);
			System.out.println("Computer has rolled number : " + guest2);
			int difference = Math.abs(guest1 - guest2);
			if (difference == 1 || difference == 5 || difference == 0) {
				
				System.out.println("You win.");
				}
			else {
				System.out.println("Computer wins.");
			}
			}
		else {
			System.err.println("Incorrect number. Only 1-6 can be entered."); 

		}
	
}
}
