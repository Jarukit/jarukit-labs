package panjarak.jarukit.lab9;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

public class AthleteFormV5 extends AthleteFormV4{
	
	public AthleteFormV5(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	protected JMenuItem colorBlue;
	protected JMenuItem colorGreen;
	protected JMenuItem colorRed;
	protected JMenuItem colorCustom;
	
	protected JMenuItem size16;
	protected JMenuItem size20;
	protected JMenuItem size24;
	protected JMenuItem sizeCustom;
	
	protected void initMenus() {
		super.initMenus();
		colorBlue = new JMenuItem("Blue");
		colorRed = new JMenuItem("Red");
		colorGreen = new JMenuItem("Green");
		colorCustom = new JMenuItem("Custom..");
		
		size16 = new JMenuItem("16");
		size20 = new JMenuItem("20");
		size24 = new JMenuItem("24");
		sizeCustom = new JMenuItem("Custom..");
	}
	protected void addMenus() {
		super.addMenus();
		
		colorMI.add(colorBlue);
		colorMI.add(colorRed);
		colorMI.add(colorGreen);
		colorMI.add(colorCustom);
		
		sizeMI.add(size16);
		sizeMI.add(size20);
		sizeMI.add(size24);
		sizeMI.add(sizeCustom);
		
	}
	public static void createAndShowGUI(){
		AthleteFormV5 AthleteForm5 = new AthleteFormV5("Athlete Form V5");
		AthleteForm5.addComponents();
		AthleteForm5.addMenus();
		AthleteForm5.setFrameFeatures();
		AthleteForm5.addListeners();
	}
	
	protected void setTextSize(int size) {
		Font font = new Font("Sans Serif", Font.BOLD, size);
		nameTextField.setFont(font);
		dateTextField.setFont(font);
		weightTextField.setFont(font);
		heightTextField.setFont(font);
		nationalityTextField.setFont(font);
		cmpTxtArea.setFont(font);
		pack(); // make it fit to components

	}
	protected void setTextColor(Color color) {
		nameTextField.setForeground(color);
		dateTextField.setForeground(color);
		weightTextField.setForeground(color);
		heightTextField.setForeground(color);
		nationalityTextField.setForeground(color);
		cmpTxtArea.setForeground(color);
	}
	
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == colorBlue) {
			setTextColor(Color.BLUE);
		} else if (src == colorGreen) {
			setTextColor(Color.GREEN);
		} else if (src == colorRed) {
			setTextColor(Color.RED);
		}
		
		// handling events to size
		if (src == size16) {
			setTextSize(16);
		} else if (src == size20) {
			setTextSize(20);
		} else if (src == size24) {
			setTextSize(24);
		}
	}
	protected void addListeners() {
		super.addListeners();
		colorBlue.addActionListener(this);
		colorGreen.addActionListener(this);
		colorRed.addActionListener(this);
		size16.addActionListener(this);
		size20.addActionListener(this);
		size24.addActionListener(this);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
