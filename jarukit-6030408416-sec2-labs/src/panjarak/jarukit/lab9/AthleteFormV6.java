package panjarak.jarukit.lab9;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class AthleteFormV6 extends AthleteFormV5{
	private static final long serialVersionUID = 1L;

	ImageIcon athleteImg;
	ImageIcon openIcon;
	ImageIcon exitIcon;
	ImageIcon saveIcon;
	JLabel imgLabel;
	JPanel imgPanel;
	public AthleteFormV6(String string) {
		super(string);
	}

	// for init the component
	protected void initComponents() {
		super.initComponents();
		athleteImg = new ImageIcon(getClass().getResource("/picture/mana.jpg"));
		imgLabel = new JLabel(null, athleteImg, JLabel.CENTER);
		imgPanel = new JPanel();
	}

	public void addComponents() {
		super.addComponents();
		initComponents();
		imgPanel.add(imgLabel);
		add(imgPanel, BorderLayout.NORTH);

	}
	
	protected void initMenus() {
		super.initMenus();
		openIcon = new ImageIcon(getClass().getResource("/picture/openIcon.png"));
		saveIcon = new ImageIcon(getClass().getResource("/picture/saveIcon.png"));
		exitIcon = new ImageIcon(getClass().getResource("/picture/quitIcon.png"));
	}
	
	protected void addMenus() {
		super.addMenus();
		openMI.setIcon(openIcon);
		saveMI.setIcon(saveIcon);
		exitMI.setIcon(exitIcon);
	}
	
	public static void createAndShowGUI(){
		AthleteFormV6 AthleteForm6 = new AthleteFormV6("Athlete Form V6");
		AthleteForm6.addComponents();
		AthleteForm6.addMenus();
		AthleteForm6.setFrameFeatures();
		AthleteForm6.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
