package panjarak.jarukit.lab9;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class AthleteFormV4 extends AthleteFormV3 implements ActionListener, ItemListener{
	
	public AthleteFormV4(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	public void actionPerformed(ActionEvent event) {
		Object src = event.getSource();
		if (src == okButton) {
			handleOKButton();
		} else if (src == cancelButton) {
			handleCancelButton();
			
		}
	}
	
	public void itemStateChanged(ItemEvent ie) {
		Object src = ie.getSource();
		if (src == typeList) {
			if(ie.getStateChange () == ItemEvent.SELECTED) {
				JOptionPane.showMessageDialog(this, "Your athelete type is changed to " + ie.getItem());
			}
			
		} else if (src == maleRdoBtn) {
			if (ie.getStateChange() == ItemEvent.SELECTED) {
				showGenderDialog("Your gender type is now changed to Male");
			}
		} else if (src == fmaleRdoBtn) {
			if (ie.getStateChange() == ItemEvent.SELECTED) {
				showGenderDialog("Your gender type is now changed to Female");
			}
		}
	}
	
	
	protected void showGenderDialog(String message) {
		// set location of dialog
	    final JOptionPane pane = new JOptionPane(message);
	    final JDialog d = pane.createDialog((JFrame)null, "Gender Info");
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = this.getWidth();
		int h = this.getHeight();
		int x = (dim.width - w)/2;
		int y = (dim.height - h)/2 + h + 25;
	    d.setLocation(x,y);
	    d.setVisible(true);

	}
	
	protected void handleOKButton() {
		String strGender;
		if(maleRdoBtn.isSelected()) {
			strGender = MALE;
		}
		else {
			strGender = FEMALE;
		}
		
		String strItem = String.valueOf(typeList.getSelectedItem());
		JOptionPane.showMessageDialog(this, "Name = " + nameTextField.getText()
		+ ", Birthdate = " + dateTextField.getText() + ", Weight = " + weightTextField.getText()
		+ ", Height = " + heightTextField.getText() + "Nationality = " + nationalityTextField.getText()
		+ "\n" + "Gender = " + strGender + "\n" + "Competition = " + cmpTxtArea.getText() + "\n" + "Type = " + strItem);
	}
	
	protected void handleCancelButton() {
		nameTextField.setText("");
		dateTextField.setText("");
		weightTextField.setText("");
		heightTextField.setText("");
		nationalityTextField.setText("");
		maleRdoBtn.setSelected(true);
		cmpTxtArea.setText("");
		typeList.setSelectedItem("Boxer");

	}
	
	protected void addListeners() {
		okButton.addActionListener(this);
		cancelButton.addActionListener(this);
		typeList.addItemListener(this);
		maleRdoBtn.addItemListener(this);
		fmaleRdoBtn.addItemListener(this);

	}
	
	public static void createAndShowGUI(){
		AthleteFormV4 athleteForm4 = new AthleteFormV4("Athlete Form V4");
		athleteForm4.addComponents();
		athleteForm4.setFrameFeatures();
		athleteForm4.addListeners();
	}
	
	

	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}


	

}
