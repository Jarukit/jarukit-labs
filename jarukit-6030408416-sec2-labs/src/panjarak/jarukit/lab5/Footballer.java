package panjarak.jarukit.lab5;

public class Footballer extends Athlete implements Playable,Movable{
	
	private static String sport = "American Football";
	private String position;
	private String team;
	
	public Footballer(String name, double weight, double height, Gender gender, String nationality, String birthdate,
			String position, String team) {
		super(name, weight, height, gender, nationality, birthdate);
		this.position = position;
		this.team = team;
	
	}
	
	

	public static String getSport() {
		return sport;
	}



	public static void setSport(String sport) {
		Footballer.sport = sport;
	}



	public String getPosition() {
		return position;
	}



	public void setPosition(String position) {
		this.position = position;
	}



	public String getTeam() {
		return team;
	}



	public void setTeam(String team) {
		this.team = team;
	}



	@Override
	public String toString() {
		return getName() + ", " + getWeight() + "kg, " + getHeight() + "m, " + getGender()
		+ ", " + getNationality() + ", " + getBirthdate() + getSport() + ", " + getPosition() + ", " +getTeam();
	}
	
	public void play() {
		System.out.println(getName() + " throws a touchdown.");
	}
	
	public void move() {
		System.out.println(getName() + " moves down the football field.");
	}
	

	
		
}


