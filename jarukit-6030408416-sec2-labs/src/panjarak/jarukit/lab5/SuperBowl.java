package panjarak.jarukit.lab5;

import java.time.LocalDate;

class SuperBowl extends Competition {

	String AFCTeam;
	String NFCTeam;
	String winningTeam;

	public SuperBowl(String name, String date, String place, String description) {
		this.name = name;
		this.date = LocalDate.parse(date, formatter);
		this.place = place;
		this.description = description;
	}

	public SuperBowl(String name, String date, String place, String 
			description, String aFCTeam, String nFCTeam,
			String winningTeam) {
		this.name = name;
		this.date = LocalDate.parse(date, formatter);
		this.place = place;
		this.description = description;
		AFCTeam = aFCTeam;
		NFCTeam = nFCTeam;
		this.winningTeam = winningTeam;
	}

	public String getAFCTeam() {
		return AFCTeam;
	}

	public void setAFCTeam(String aFCTeam) {
		AFCTeam = aFCTeam;
	}

	public String getNFCTeam() {
		return NFCTeam;
	}

	public void setNFCTeam(String nFCTeam) {
		NFCTeam = nFCTeam;
	}

	public String getWinningTeam() {
		return winningTeam;
	}

	public void setWinningTeam(String winningTeam) {
		this.winningTeam = winningTeam;
	}

	public void setDescriptionAndRules() {
		System.out.println("===== Begin: Description and Rules ===========");
		System.out.println(getName() + " is played between the champions of the National Football Conference (NFC)");
		System.out.println("and the American Football Conference (AFC).");
		System.out.println("The game play in four quarters while each quarter takes about 15 minutes.");
		System.out.println("===== End: Discription and Rules ===========");
	}

	public String toString() {
		return getName() + "(" + description + ")" + " was held on "
				+ date + " in " + place + "." + "\n" + "It was the game between " + AFCTeam + " vs. " + NFCTeam + "."
				+ "\n" + "The winner was " + getWinningTeam() + ".";
	}

}
