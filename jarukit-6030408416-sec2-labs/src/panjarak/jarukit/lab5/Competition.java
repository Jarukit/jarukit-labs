package panjarak.jarukit.lab5;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public abstract class Competition {
	protected String name;
	protected LocalDate date;
	protected String place;
	protected String description;
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public abstract void setDescriptionAndRules();
		

}
