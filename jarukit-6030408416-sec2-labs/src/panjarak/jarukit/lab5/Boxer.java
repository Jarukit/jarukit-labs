package panjarak.jarukit.lab5;
/**
* Boxer is program which is a subclass of Athlete and add 2 private instance variables,Create (set and get) and constructor before called java.lang.Object.toString().
*
* @author  Jarukit Panjarak
* @version 1.0
* @since   2017-02-5 
*/
public class Boxer extends Athlete implements Playable,Movable{
	// this is 2 private instance variables. 
	static private String sport = "Boxer";
	private String division;
	private String golveSize;
	
	// constructor that accepts name, weight, height, gender, nationality, birthdate,  racketLength and worldRanking.
	public Boxer(String name, double weight, double height, Gender gender, String nationality, String birthdate, String division, String golveSize) {
		super(name, weight, height, gender, nationality, birthdate);
		// TODO Auto-generated constructor stub
		this.division = division;
		this.golveSize = golveSize;
	}
	
	// this is public methods to set and get each information.
	
	public static String getSport() {
		return sport;
	}
	public static void setSport(String sport) {
		Boxer.sport = sport;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getGolveSize() {
		return golveSize;
	}
	public void setGolveSize(String golveSize) {
		this.golveSize = golveSize;
	}
	//Override a method called java.lang.Object.toString() to display the information.
	@Override
	public String toString() {
		return getName() + ", " + getWeight() + "kg, " + getHeight() + "m, " + getGender()
		+ ", " + getNationality() + ", " + getBirthdate() + ", " + getSport() + ", " + getDivision() + ", " + getGolveSize();
	}
	
	public void play() {
		System.out.println(getName() + " throws a punch.");
	}
	
	public void move() {
		System.out.println(getName() + " moves around a boxing ring.");
	}
}
