package panjarak.jarukit.lab5;
/**
* BadmintonPlayer is program which is a subclass of Athlete and add 2 private instance variables,Create (set and get) and constructor before called java.lang.Object.toString().
*
* @author  Jarukit Panjarak
* @version 1.0
* @since   2017-02-5 
*/

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;

public class BadmintonPlayer extends Athlete implements Playable,Movable{
	// this is 2 private instance variables. 
	private static String sport = "Badminton";
	private double racketLength;
	private int worldRanking;
	
	// constructor that accepts name, weight, height, gender, nationality, birthdate,  racketLength and worldRanking.
	public BadmintonPlayer(String name, double weight, double height, Gender gender, String nationality,
			String birthdate, double racketLength, int worldRanking) {
		super(name, weight, height, gender, nationality, birthdate);
		// TODO Auto-generated constructor stub
		this.racketLength = racketLength;
		this.worldRanking = worldRanking;
	}
	// this is public methods to set and get each information.
	
	public static String getSport() {
		return sport;
	}
	public static void setSport(String sport) {
		BadmintonPlayer.sport = sport;
	}

	public double getRacketLength() {
		return racketLength;
	}
	public void setRacketLength(double racketLength) {
		this.racketLength = racketLength;
	}
	public int getWorldRanking() {
		return worldRanking;
	}
	public void setWorldRanking(int worldRanking) {
		this.worldRanking = worldRanking;
	}
	
	//Override a method called java.lang.Object.toString() to display the information.
	@Override
	public String toString() {
		return getName() + ", " + getWeight() + "kg, " + getHeight() + "m, " + getGender()
				+ ", " + getNationality() + ", " + getBirthdate() + getRacketLength() +"cm, " + "rank:" + getWorldRanking();
	}
	
	//this is compareAge() that accepts another athlete then compare the age of an instance athlete to that of the other athlete input as parameter.
	public void compareAge(Athlete x) {
		LocalDate dateBefor = x.getBirthdate();
		LocalDate dateAfter = getBirthdate();
		long year = ChronoUnit.YEARS.between(dateBefor, dateAfter);
        
		if (year < 0){
            System.out.println ( x.getName ()+" is " + Math.abs(year) + " years older than " + getName () );
        }
        else {
            System.out.println ( getName ()+" is " + year + " years older than " + x.getName () );
        }
	}
	
	public void play() {
		System.out.println(getName() + " hits a shuttlecock.");
	}
	
	public void move() {
		System.out.println(getName() + " moves around badminton court.");
	}
}
