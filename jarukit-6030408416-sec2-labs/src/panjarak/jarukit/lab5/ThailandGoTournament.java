package panjarak.jarukit.lab5;

import java.time.LocalDate;

public class ThailandGoTournament extends Competition{
	String winnerDesc;
	
	public ThailandGoTournament(String name, String date, String place) {
		this.name = name;
		this.date = LocalDate.parse(date, formatter);
		this.place = place;
	}
	
	
	
	public String getWinner() {
		return winnerDesc;
	}



	public void setWinner(String winnerDesc) {
		this.winnerDesc = winnerDesc;
	}



	public void setDescriptionAndRules() {
		System.out.println("===== Begin: Description and Rules ===========");
		System.out.println("The competition is open for all students and people");
		System.out.println("The competition is devided into four categories");
		System.out.println("1.Hight Dan (3 Dan and above)");
		System.out.println("2.Low Dan (1-2 Dan)");
		System.out.println("3.Hight Kyu (1-4 Kyu)");
		System.out.println("4.Low Kyu (5-8 Kyu)");
		System.out.println("===== End: Discription and Rules ===========");

	}
	
	public String toString() {
		return name + "was held on "
				+ date + " in " + place + "." + "\n" + "Some winners are " + winnerDesc;
	}
}


