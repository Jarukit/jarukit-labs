package panjarak.jarukit.lab10;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.util.Random;

import javax.swing.JPanel;

public class MovingPongGamePanelV3 extends JPanel implements Runnable, KeyListener {

	private static final long serialVersionUID = 1L;
	protected MovingBall movingBall;
	private Thread running;
	private Random rand;
	private int ballR = 20;
	protected MovablePongPaddle movableRightPad;
	protected MovablePongPaddle movableLeftPad;
	protected Rectangle2D.Double box;
	protected Integer player1Score;
	protected Integer player2Score;
	protected String winner;

	public MovingPongGamePanelV3() {
		super();

		addKeyListener(this);
		setFocusable(true);

		setBackground(Color.BLACK);

		// initialize the pads
		movableLeftPad = new MovablePongPaddle(0, SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH,
				PongPaddle.HEIGHT);
		movableRightPad = new MovablePongPaddle(SimpleGameWindow.WIDTH - PongPaddle.WIDTH,
				SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH, PongPaddle.HEIGHT);
		resetBall();

		// initialize the ball
		box = new Rectangle2D.Double(0, 0, SimpleGameWindow.WIDTH, SimpleGameWindow.HEIGHT);

		// set the player scores
		player1Score = 0;
		player2Score = 0;
		winner = "";

		running = new Thread(this);
		running.start();
	}

	private void resetBall() {
		int[] randomArray = { -3, -2, -1, 1, 2, 3 };
		rand = new Random();
		int randX = rand.nextInt(6);
		int randY = rand.nextInt(6);
		movingBall = new MovingBall(SimpleGameWindow.WIDTH / 2 - ballR, SimpleGameWindow.HEIGHT / 2 - ballR, ballR,
				randomArray[randX], randomArray[randY]);

	}

	@Override
	public void run() {

		while (true) {

			moveBall();
			repaint();

			if (player1Score == 10) {
				winner = "Player 1 Win";
			} else if (player2Score == 10) {
				winner = "Player 2 Win";
			}

			this.getToolkit().sync(); // to flush the graphic buffer

			// Delay
			try {
				// try to adjust the number here to have a smooth
				// running ball on your machine
				Thread.sleep(15);
			} catch (InterruptedException ex) {
				System.err.println(ex.getStackTrace());
			}
			if (winner != "") {
				break;
			}
		}
	}

	// update position of the ball
	private void moveBall() {
		movingBall.move();
		boolean collideRightXAxis = (movingBall.x + ballR * 2 > (SimpleGameWindow.WIDTH - movableRightPad.width))
				&& movingBall.x < SimpleGameWindow.WIDTH;
		boolean collideLeftXAxis = (movingBall.x < movableRightPad.width) && movingBall.x > 0;
		boolean collideRightYAxis = (movingBall.y < movableRightPad.y + movableRightPad.height)
				&& (movingBall.y + ballR * 2 > movableRightPad.y);
		boolean collideLeftYAxis = (movingBall.y < movableLeftPad.y + movableLeftPad.height)
				&& (movingBall.y + ballR * 2 > movableLeftPad.y);
		boolean collideRightPad = collideRightXAxis && collideRightYAxis;
		boolean collideLeftPad = collideLeftXAxis && collideLeftYAxis;
		if (collideRightPad || collideLeftPad) {
			movingBall.setVelX(-movingBall.getVelX());
		}

		if (movingBall.x > SimpleGameWindow.WIDTH) {
			player1Score += 1;
			resetBall();
		} else if (movingBall.x + ballR * 2 < 0) {
			player2Score += 1;
			resetBall();
		}

		if (movingBall.y + ballR * 2 > SimpleGameWindow.HEIGHT || movingBall.y < 0) {
			movingBall.setVelY(-movingBall.getVelY());
		}

		if (player1Score == 10) {
			winner = "Player 1 Win";
		} else if (player2Score == 10) {
			winner = "Player 1 Win";
		}
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D) g;

		g2.setColor(Color.WHITE);

		// draw the middle line
		g2.drawLine(SimpleGameWindow.WIDTH / 2, 0, SimpleGameWindow.WIDTH / 2, SimpleGameWindow.HEIGHT);

		// draw line on the left
		g2.drawLine(movableLeftPad.getW(), 0, movableLeftPad.getW(), SimpleGameWindow.HEIGHT);

		// draw line on the right
		g2.drawLine(SimpleGameWindow.WIDTH - movableRightPad.getW(), 0, SimpleGameWindow.WIDTH - movableRightPad.getW(),
				SimpleGameWindow.HEIGHT);

		// Draw the score
		g2.setFont(new Font(Font.SERIF, Font.BOLD, 48));
		g2.drawString(player1Score.toString(), SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT / 5);
		g2.drawString(player2Score.toString(), 3 * SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT / 5);
		g2.drawString(winner, SimpleGameWindow.WIDTH / 3, SimpleGameWindow.HEIGHT / 3);

		// Draw the paddles
		g2.fill(movableLeftPad);
		g2.fill(movableRightPad);

		// draw the ball
		g2.fill(movingBall);

		// draw the box
		g2.draw(box);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_UP)
			movableRightPad.moveUp();
		else if (e.getKeyCode() == KeyEvent.VK_DOWN)
			movableRightPad.moveDown();
		else if (e.getKeyCode() == KeyEvent.VK_W)
			movableLeftPad.moveUp();
		else if (e.getKeyCode() == KeyEvent.VK_S)
			movableLeftPad.moveDown();
		repaint();

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}
}