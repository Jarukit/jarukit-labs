package panjarak.jarukit.lab6.advanced;

import panjarak.jarukit.lab6.Author;

public class Book {
	private String name;
	private Author[] authors;
	private double price;
	private int qty = 0;
	public Book(String name, Author[] authors, double price) {
		super();
		this.name = name;
		this.authors = authors;
		this.price = price;
	}
	public Book(String name, Author[] authors, double price, int qty) {
		super();
		this.name = name;
		this.authors = authors;
		this.price = price;
		this.qty = qty;
	}

	// Getter and Setter
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public String getName() {
		return name;
	}

	public Author[] getAuthors() {
		return authors;
	}

	public String getAuthorToString() {
		StringBuffer authorNames = new StringBuffer("{");
		for (int i = 0; i < authors.length; i++) {
			authorNames.append(authors[i].toString());
			if (i == authors.length - 1) {
				authorNames.append("}");
			} else {
				authorNames.append("\n");
			}
		}
		return authorNames.toString();
	}

	public String getAuthorNames() {
		StringBuffer names = new StringBuffer();
		for (int i = 0; i < authors.length; i++) {
			names.append(authors[i].getName());
			if (i == authors.length - 1) {
				names.append("\n");
			} else {
				names.append(", ");
			}
		}
		return names.toString();
	}

	@Override
	public String toString() {
		return "Book [name=" + name + ", authors =" + getAuthorToString() + ", price =" + price + ", qty =" + qty + "]";
	}
}