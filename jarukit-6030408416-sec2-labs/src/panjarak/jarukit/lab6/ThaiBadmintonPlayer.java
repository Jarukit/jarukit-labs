package panjarak.jarukit.lab6;
import panjarak.jarukit.lab5.*;

public class ThaiBadmintonPlayer extends BadmintonPlayer implements Playable,Movable{
	private String equipment = "�١����";
	
	public ThaiBadmintonPlayer(String name, double weight, double height, Gender gender,
			String birthdate, double racketLength, int worldRanking) {
		super(name, weight, height, gender, "Thai", birthdate, racketLength, worldRanking);
		// TODO Auto-generated constructor stub
	}


	public String getEquipment() {
		return equipment;
	}

	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}
	
	
	public String toString() {
		return getName() + ", " + getWeight() + "kg, " + getHeight() + "m, " + getGender()
		+ ", " + getNationality() + ", " + getBirthdate() + getRacketLength() +"cm, " + "rank:" + getWorldRanking() + " " +  "equipment:" + getEquipment();
	}
	
	public void play() {
		super.play();
		System.out.println(getName() + " hits a " + getEquipment());
	}
}
