package panjarak.jarukit.lab4;

public class Boxer extends Athlete {
	static private String sport = "Boxer";
	private String division;
	private String golveSize;
	public Boxer(String name, double weight, double height, Gender gender, String nationality, String birthdate, String division, String golveSize) {
		super(name, weight, height, gender, nationality, birthdate);
		// TODO Auto-generated constructor stub
		this.division = division;
		this.golveSize = golveSize;
	}
	public static String getSport() {
		return sport;
	}
	public static void setSport(String sport) {
		Boxer.sport = sport;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getGolveSize() {
		return golveSize;
	}
	public void setGolveSize(String golveSize) {
		this.golveSize = golveSize;
	}
	
	@Override
	public String toString() {
		return getName() + ", " + getWeight() + "kg, " + getHeight() + "m, " + getGender()
		+ ", " + getNationality() + ", " + getBirthdate() + ", " + getSport() + ", " + getDivision() + ", " + getGolveSize();
	}
	
}
