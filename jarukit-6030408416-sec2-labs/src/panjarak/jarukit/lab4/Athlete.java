package panjarak.jarukit.lab4;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Athlete {
	// this is variables.
	private String name;
	private double weight;
	private double height;
	private Gender gender;
	private String nationality;
	private LocalDate birthdate;
	
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	// this is constructor which accepts values for the fields.
	
	public Athlete(String name, double weight, double height, Gender gender, String nationality, String birthdate) {
		super();
		this.name = name;
		this.weight = weight;
		this.height = height;
		this.gender = gender;
		this.nationality = nationality;
		this.birthdate = LocalDate.parse(birthdate, formatter);
	}
	// Create public methods to set and get each information.
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationallity(String nationallity) {
		this.nationality = nationallity;
	}
	public LocalDate getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(String birthdate) {
		this.birthdate = LocalDate.parse(birthdate, formatter);
	}
	
	//Override a method called java.lang.Object.toString() to display the information.
	@Override
	public String toString() {
		return "Athlete [" + name + ", " + weight + "kg, "+  height + "m, " + gender +", " + nationality + ", " +  birthdate + "]";
	}
	
	

}
