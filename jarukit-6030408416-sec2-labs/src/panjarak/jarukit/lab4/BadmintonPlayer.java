package panjarak.jarukit.lab4;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;

public class BadmintonPlayer extends Athlete {
	private static String sport = "Badminton";
	private double racketLength;
	private int worldRanking;
	public BadmintonPlayer(String name, double weight, double height, Gender gender, String nationality,
			String birthdate, double racketLength, int worldRanking) {
		super(name, weight, height, gender, nationality, birthdate);
		// TODO Auto-generated constructor stub
		this.racketLength = racketLength;
		this.worldRanking = worldRanking;
	}
	public static String getSport() {
		return sport;
	}
	public double getRacketLength() {
		return racketLength;
	}
	public void setRacketLength(double racketLength) {
		this.racketLength = racketLength;
	}
	public int getWorldRanking() {
		return worldRanking;
	}
	public void setWorldRanking(int worldRanking) {
		this.worldRanking = worldRanking;
	}
	
	@Override
	public String toString() {
		return getName() + ", " + getWeight() + "kg, " + getHeight() + "m, " + getGender()
				+ ", " + getNationality() + ", " + getBirthdate() + getRacketLength() +"cm, " + "rank:" + getWorldRanking();
	}
	
	public void compareAge(Athlete x) {
		LocalDate dateBefor = x.getBirthdate();
		LocalDate dateAfter = getBirthdate();
		long year = ChronoUnit.YEARS.between(dateBefor, dateAfter);
        
		if (year < 0){
            System.out.println ( x.getName ()+" is " + Math.abs(year) + " years older than " + getName () );
        }
        else {
            System.out.println ( getName ()+" is " + year + " years older than " + x.getName () );
        }
	}
	
}
