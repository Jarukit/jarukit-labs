package panjarak.jarukit.lab8;


import java.awt.*;

import javax.swing.*;

public class SimpleGameWindow extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	private int WIDTH = 650;
	private int HIGTH = 500;

	  public SimpleGameWindow(String title) {
		super(title);
	}

	public static void createAndShowGUI(){
	    // create our jframe as usual
	    SimpleGameWindow window = new SimpleGameWindow("My Simple Game Window");
	    window.setFrameFeatures();
	  }
	  
	  public void setFrameFeatures() {
		    setSize(WIDTH,HIGTH);
		    setLocationRelativeTo(null);
		    setVisible(true);
		    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  }
	  
	  
	  public static void main(String[] args)
	  {
	    // schedule this for the event dispatch thread (edt)
	    SwingUtilities.invokeLater(new Runnable()
	    {
	      public void run()
	      {
	    	  createAndShowGUI();
	      }
	    });
	  }
	  
	  
	  
	  

}
