package panjarak.jarukit.lab8;

import javax.swing.SwingUtilities;

public class TestWrapAroundBall extends SimpleGameWindow {
	
	private static final long serialVersionUID = 1L;

	public TestWrapAroundBall(String title) {
		super(title);
	}

	protected void addComponents() {
		WrapAroundBall panel = new WrapAroundBall();
		setContentPane(panel);
	}

	public static void createAndShowGUI() {
		TestWrapAroundBall window = new TestWrapAroundBall("Test Wrap Around Ball");
		window.setFrameFeatures();
		window.addComponents();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
