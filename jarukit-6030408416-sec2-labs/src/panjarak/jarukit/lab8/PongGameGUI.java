package panjarak.jarukit.lab8;

import javax.swing.SwingUtilities;

public class PongGameGUI extends SimpleGameWindow{

	private static final long serialVersionUID = 1L;

	  public PongGameGUI(String title) {
		super(title);
	}

	public static void createAndShowGUI(){
	    // create our jframe as usual
		PongGameGUI pg = new PongGameGUI("My Simple Game Window");
		pg.addComponents();
		pg.setFrameFeatures();
	  }
	  
	
	public void addComponents() {
		PongPanel pong = new PongPanel();
		setContentPane(pong);
	}
	
	@Override
	public void setFrameFeatures() {
		super.setFrameFeatures();
		pack();
	}
	  
	  
	  public static void main(String[] args)
	  {
	    // schedule this for the event dispatch thread (edt)
	    SwingUtilities.invokeLater(new Runnable()
	    {
	      public void run()
	      {
	    	  createAndShowGUI();
	      }
	    });
	  }
	  
}

