package panjarak.jarukit.lab8;

import java.awt.*;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

public class BouncyBall  extends JPanel implements Runnable {
	private static final long serialVersionUID = 1L;
	
	Thread running;
	MovingBall ball;
	Rectangle2D.Double box;
	private int WIDTH = 650;
	private int HIGTH = 500;
	int velocityX = 2;
	int velocityY = 2;
	int directionX = 1;
	int directionY = 1;
	
	public BouncyBall() {
		setBackground(Color.GREEN);
		ball = new MovingBall(WIDTH / 2, HIGTH / 2, 20, velocityX, velocityY);
		box = new Rectangle2D.Double(0, 0, WIDTH , HIGTH );
		running = new Thread(this);
		running.start();
	}
	
	@Override
	public void run() {

		while(true){
			
			// update coordination
			ball.x = ball.x + (ball.getxAxis() * directionX);
			if (ball.x > WIDTH || ball.x < 1) {
				directionX *= -1;
			}
			
			ball.y = ball.y + (ball.getyAxis() * directionY);
			if (ball.y > HIGTH || ball.y < 1) {
				directionY *= -1;
			}

			repaint();
			this.getToolkit().sync(); 
			
			// Delay
			try
			{
				
				Thread.sleep(20);
			}
			catch(InterruptedException ex)
			{
				System.err.println(ex.getStackTrace());
			}
		}
	}
	
	public void paint(Graphics g){
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.YELLOW);
		g2d.fill(ball);
	}
}
