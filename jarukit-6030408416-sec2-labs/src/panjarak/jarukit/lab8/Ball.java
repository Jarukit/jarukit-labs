package panjarak.jarukit.lab8;

import java.awt.geom.Ellipse2D;

public class Ball extends Ellipse2D.Double{
	
	private static final long serialVersionUID = 1L;
	
	private int r;

	public int getR() {
		return r;
	}

	public void setR(int r) {
		this.r = r;
	}
	
	public Ball(int x, int y, int r){
		super(x, y, 2*r, 2*r);
	}
	
	
}
