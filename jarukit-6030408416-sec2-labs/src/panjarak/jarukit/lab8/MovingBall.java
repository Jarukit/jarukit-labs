package panjarak.jarukit.lab8;

public class MovingBall extends Ball{
	
	private static final long serialVersionUID = 1L;
	
	private int xAxis;
	private int yAxis;
	
	public MovingBall(int x, int y, int r, int xAxis, int yAxis) {
		super(x, y, r);
		
		this.xAxis = xAxis;
		this.yAxis = yAxis;
	}

	public int getxAxis() {
		return xAxis;
	}

	public void setxAxis(int xAxis) {
		this.xAxis = xAxis;
	}

	public int getyAxis() {
		return yAxis;
	}

	public void setyAxis(int yAxis) {
		this.yAxis = yAxis;
	}
	
	
}
