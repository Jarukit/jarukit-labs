package panjarak.jarukit.lab8;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

import javax.swing.JPanel;

public class PongPanel extends JPanel{
	/**
	 * 
	 */
	private Line2D leftLine, rightLine;
	private static final long serialVersionUID = 1L;
	private PongPaddle left;
	private PongPaddle right;
	private int score1;
	private int score2;
	private Ball ball;
	private final int PANE_WIDTH = 650;
	private final int PANE_HEIGHT = 500;
	private final int PADDLE_WIDTH = 15;
	private final int PADDLE_HEIGHT = 80;

	private final int PADDLE_POSITION_Y = (PANE_HEIGHT/2) - (PADDLE_HEIGHT/2);
	private final int PADDLE_POSITION_X = PANE_WIDTH - PADDLE_WIDTH;
	private final int BALL_POSITION_Y = (PANE_HEIGHT/2) - 20;
	private final int BALL_POSITION_X = (PANE_WIDTH/2) - 20;
	private final int DRAWLINE_POSITION_X = 650 - PADDLE_WIDTH;
	private final int DRAWLINE_POSITION_BALL = PANE_WIDTH/2;
	
	public PongPanel() {
		
		setBackground(Color.black);	
		left = new PongPaddle(0, PADDLE_POSITION_Y, PADDLE_WIDTH, PADDLE_HEIGHT);
		right = new PongPaddle(PADDLE_POSITION_X, PADDLE_POSITION_Y, PADDLE_WIDTH, PADDLE_HEIGHT);
		score1 = 0;
		score2 = 0;
		ball = new Ball(BALL_POSITION_X, BALL_POSITION_Y, 20);	
		
		
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(Color.white);
		g2d.fill(left);
		g2d.fill(right);
		g2d.fill(ball);
		g2d.drawLine(PADDLE_WIDTH, 0, PADDLE_WIDTH, PANE_HEIGHT);
		g2d.drawLine(DRAWLINE_POSITION_X, 0, DRAWLINE_POSITION_X, PANE_HEIGHT);
		g2d.drawLine(DRAWLINE_POSITION_BALL, 0, DRAWLINE_POSITION_BALL, PANE_HEIGHT);
		String score1Str = Integer.toString(score1);
		g2d.setFont(new Font("Serif", Font.BOLD, 48));
		g2d.drawString(score1Str, PANE_WIDTH/4, PANE_HEIGHT/6);
		String score2Str = Integer.toString(score2);
		g2d.drawString(score2Str, (3*PANE_WIDTH/4), PANE_HEIGHT/6);
	
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(PANE_WIDTH, PANE_HEIGHT);
	}
}
