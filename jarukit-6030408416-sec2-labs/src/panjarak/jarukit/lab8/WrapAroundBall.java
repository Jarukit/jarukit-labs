package panjarak.jarukit.lab8;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;

public class WrapAroundBall extends JPanel implements Runnable{
	
	private static final long serialVersionUID = 1L;
	
	MovingBall yellowBall, redBall, greenBall, blackBall, whiteBall;
	Thread running;
	private int WIDTH = 650;
	private int HIGTH = 500;
	int velocityX = 5;
	int velocityY = 5;
	
	public WrapAroundBall() {
		setBackground(Color.BLUE);
		//initialize the position of the ball
		
		yellowBall = new MovingBall(WIDTH / 2, 0, 20, velocityX, velocityY);
		redBall = new MovingBall(0, WIDTH / 2, 20, velocityX, velocityY);
		greenBall = new MovingBall(0, 0, 20, velocityX, velocityY);
		blackBall = new MovingBall(WIDTH / 2 , HIGTH, 20, velocityX, velocityY);
		whiteBall =  new MovingBall(WIDTH , HIGTH / 2 , 20, velocityX, velocityY);
		
		running = new Thread(this);
		running.start();
	}
	
	public void run() {
		while(true) {
			// update the coordinate of the ball here

			// yellow move
			yellowBall.y = yellowBall.y + yellowBall.getyAxis();
			if (yellowBall.y > HIGTH) {
				yellowBall.y = 0;
			}

			// red move
			redBall.x = redBall.x + redBall.getxAxis();
			if (redBall.x > WIDTH) {
				redBall.x = 0;
			}

			// green move
			greenBall.x = greenBall.x + greenBall.getxAxis();
			greenBall.y = greenBall.y + greenBall.getyAxis();
			if (greenBall.y > HIGTH) {
				greenBall.y = 0;
			}
			if (greenBall.x > WIDTH) {
				greenBall.x = 0;
			}

			// black move
			blackBall.y = blackBall.y - blackBall.getyAxis();
			if (blackBall.y < 0) {
				blackBall.y = HIGTH;
			}

			// white move
			whiteBall.x = whiteBall.x - whiteBall.getxAxis();
			if (whiteBall.x < 0) {
				whiteBall.x = WIDTH;
			}
			
			repaint();
			this.getToolkit().sync(); //to flush the graphic buffer
			
			// Delay
			try {
				// try to adjust the number here to have a smooth
				// runnung ball on your machine
				Thread.sleep(20);
			}
			catch(InterruptedException ex) {
				System.err.println(ex.getStackTrace());
			}
		}
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.YELLOW);
		g2d.fill(yellowBall);
		g2d.setColor(Color.RED);
		g2d.fill(redBall);
		g2d.setColor(Color.GREEN);
		g2d.fill(greenBall);
		g2d.setColor(Color.BLACK);
		g2d.fill(blackBall);
		g2d.setColor(Color.WHITE);
		g2d.fill(whiteBall);
	}
	
	
}
