package panjarak.jarukit.lab8;

import javax.swing.SwingUtilities;
public class TestBouncyBall extends SimpleGameWindow {

	private static final long serialVersionUID = 1L;

	public TestBouncyBall(String title) {
		super(title);
	}

	protected void addComponents() {
		BouncyBall panel = new BouncyBall();
		setContentPane(panel);
	}

	public static void createAndShowGUI() {
		TestBouncyBall window = new TestBouncyBall("Test Bouncy Ball");
		window.setFrameFeatures();
		window.addComponents();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
